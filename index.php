<!DOCTYPE html>
<?php include "zuoraGenerateSignature.php" ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My Payment Capture Page</title>

    <script type="text/javascript" src="<?php echo $zuoraConfig['js']; ?>"></script>
</head>
<body>


<script>

    // the callback function handles the response from the iframe (this occurs when submitEnabled=true only)
    // otherwise the iframe will redirect
    // see: https://knowledgecenter.zuora.com/CA_Commerce/G_Hosted_Commerce_Pages/B_Payment_Pages_2.0/H_Integrate_Payment_Pages_2.0/A_Advanced_Integration_of_Payment_Pages_2.0#Implement_the_Callback_Page
    function callback(response) {
      console.log(response);

        // write the value to the document
        var paymentDiv = document.getElementById("zuora_payment");
        paymentDiv.appendChild(document.createTextNode("Token Generated: " + response.refId));

        // typically here you would send the token and other detail back to your server
    }

    var prepopulateFields = {

          // see the following document for what you can pre-populate
          // https://knowledgecenter.zuora.com/CA_Commerce/G_Hosted_Commerce_Pages/B_Payment_Pages_2.0/K_Payment_Pages_2.0_Form_Fields
    };


    // set up the parameters based on the Zuora call.
    var params = {
        tenantId:  "<?php echo $zuoraSignature["tenantId"] ?>",
        id:        "<?php echo $zuoraConfig['pageId'] ?>",
        token:     "<?php echo $zuoraSignature["token"] ?>",
        signature: "<?php echo $zuoraSignature["signature"] ?>",
        style:     "overlay",   /* options are overlay or inline */
        key:       "<?php echo $zuoraSignature["key"] ?>",
        submitEnabled: "true",  /* if true the callback javascript function is used, otherwise the page does a redirect within the iframe */
        locale:    "en_GB",
        url:       "<?php echo $zuoraConfig["host"] ?>/apps/PublicHostedPageLite.do"
    };


</script>


<div id="zuora_payment" ></div>

<script>

    // don't forget that you can only call Z.render when the div exists.
    // in this case by putting the script tag after the div.
    Z.render(
        params,
        prepopulateFields,
        callback
    );

</script>


</body>
</html>
