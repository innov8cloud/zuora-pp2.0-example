Example Zuora PP2.0 iframe.
===========================

Steps for implementing the reference Payment Page implementation.  

Prerequisites:-

1. Install PHP.  http://php.net/manual/en/install.php

Steps:-

1. Create a Zuora Hosted Page.


    [See Zuora Instructions](https://knowledgecenter.zuora.com/CA_Commerce/G_Hosted_Commerce_Pages/B_Payment_Pages_2.0/B_Configure_Payment_Pages_2.0/Configure_Credit_Card_Type_Payment_Pages_2.0)
   
    ```
    The important fields to note are:-
    
      Hosted Domain
        Set this to http://localhost:9000
      Callback Path
        This can be anything and is not used in this demo, as the javascript handles the response.
    
      Once saved, get the pageid of the hosted page, you will need to it later.
    ```

2. Check out this code 

    ```
    git clone git@gitlab.com:danielmorton/zuora-example-pp20.git
    ```

3. Run the demo

    ```
    cd zuora-example-pp20
    USERNAME=test@examle.com PASSWORD=pass PAGEID=1234 php -S localhost:9000 -t .
    ```

4. Try it

    http://localhost:9000



