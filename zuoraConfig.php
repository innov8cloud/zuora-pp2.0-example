<?php

    function default_value($var, $default) {
        if (empty($var)) {
            return $default;
        }

        return $var;
    }


    // hold the config options
    $zuoraConfig = array (
        "host" =>  default_value(getenv('HOST'), "https://apisandbox.zuora.com"),
        "tenant_username" => getenv('USERNAME'),
        "tenant_password" => getenv('PASSWORD'),
        "pageId" => getenv('PAGEID')
    );

    // work out which js file to use
    $host = parse_url ( $zuoraConfig['host'] );
    error_log("Setting config for :" . $host['host']);

    if ( $host['host'] === 'www.zuora.com' ) {
        $zuoraConfig['js'] = 'https://static.zuora.com/Resources/libs/hosted/1.3.0/zuora-min.js';
        $zuoraConfig['rest'] = 'https://api.zuora.com/rest/';
    } else if ( $host['host'] === 'apisandbox.zuora.com' ) {
        $zuoraConfig['js'] = 'https://apisandboxstatic.zuora.com/Resources/libs/hosted/1.3.0/zuora-min.js';
        $zuoraConfig['rest'] = 'https://apisandbox-api.zuora.com/rest/';
    } else {    
        // might be services..
        $zuoraConfig['js'] = 'https://' + $host['host'] + '/Resources/libs/hosted/1.3.0/zuora-min.js';
        $zuoraConfig['rest'] = 'https://' + $host['host'] + '/apps/';
    }
  
    error_log("Config is:" . print_r($zuoraConfig,true));

?>


