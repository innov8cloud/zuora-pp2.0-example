<?php
    // make a call to the Zuora server to get the rsa-signature

    include "zuoraConfig.php";


    $rsaRequest = array (
        "uri" => $zuoraConfig['host'] . "/apps/PublicHostedPageLite.do",
        "method" => "POST",
        "pageId" => $zuoraConfig['pageId']

    );

    $postBody = json_encode($rsaRequest);
    $ch = curl_init();

    $rsaUrl  = $zuoraConfig['rest'] . "v1/rsa-signatures";

    curl_setopt($ch, CURLOPT_URL, $rsaUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt($ch, CURLOPT_POST,           1 );
    curl_setopt($ch, CURLOPT_POSTFIELDS,     $postBody );
    curl_setopt($ch, CURLOPT_HTTPHEADER,     array(
        "Content-type: application/json;charset=\"utf-8\"",
        //"Content-length: ".strlen($postBody),
        //"Authorization: Basic " . base64_encode($credentials)
        "apiAccessKeyId: " . $zuoraConfig['tenant_username'],
        "apiSecretAccessKey: " . $zuoraConfig['tenant_password']
    ));

    error_log("Sending signature request to Zuora. [" . $rsaUrl . ']');
    error_log(" body is:" . json_encode($rsaRequest, JSON_PRETTY_PRINT));

    $response = curl_exec($ch);
    $responseCode = curl_getinfo ( $ch , CURLINFO_HTTP_CODE );
    curl_close($ch);

    error_log(" Got response: (" . $responseCode . "):  " . $response);
    $zuoraSignature = json_decode($response, true);

    if (!$zuoraSignature["success"]) {
        $errorMessage = $zuoraSignature["reasons"][0]["message"];
        error_log(" These is an error : " . $errorMessage);
    }

?>


